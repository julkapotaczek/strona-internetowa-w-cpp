
Czy warto tworzyć strony internetowe z wykorzystaniem języka c++, jako głównego narzędzia do opisu logiki witryny?

# Uruchomienie lokalne

## Wymagania techniczne

- docker-engine 1.12+

Przykładowe strony zostały wykonane w oparciu o środowisko Docker, pozwalające na uruchomianie maszyn wirtualnych z wykorzystaniem przygotowanych szablonów, opisanych w plikach Dockerfile. Zastosowanie tego systemu było wymagane ze względu na potrzebę odczytu zmiennych środowiskowych, tworzonych przez serwer Apache.

## Kompilacja i uruchomienie
Dla każdego z przykładów:

    $ docker build -t WEBSITE-CPP .
    $ docker run --rm -p 80:80 WEBSITE-CPP

# Utworzone przykłady

### Przykład 1

 - [Strona komunikatu 200](example.com/index.cgi)
 - [Strona błędu 404](example.com/error.cgi)

### Przykład 2

 - [Plik index.html](example.com/index.cgi?index)
 - [Plik about.html](example.com/index.cgi?about)

# Struktura przykładów
Oba foldery mają następującą strukturę:

 - Dockerfile - plik opisujący maszynę wirtualną (kontener) serwera Apache
 - https.conf - plik konfiguracyjny serwera Apache
 - src - katalog zawierający pliki źródłowe w języku c++ oraz plik Makefile, będący skryptem do kompilacji
 - public - katalog zawierający pliki statyczne (css, html) dla serwera Apache
 
# Wnioski
Tworzenie stron internetowych w języku c++ obarczone jest szeregiem niedogodności ze względu na potrzebę kompilacji plików źródłowych przy każdorazowym uruchomieniu programu. Stosowanie języka uruchamianego na serwerze daje jednak możliwość tworzenia treści dynamicznych, zależnych od czynników zewntętrznych oraz ukrycia elementów skryptowych, które powinny pozostać niejawne.
