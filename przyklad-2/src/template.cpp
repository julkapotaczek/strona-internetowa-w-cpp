#include "template.hpp"
using namespace std;
void Template::header(std::string title, std::string path) {
    cout << "Content-type: text/html \r\n\r\n"
    << "<!DOCTYPE html>\n"
    << "<html lang=\"pl\">\n"
    << "<head>\n"
    << "<meta charset=\"utf-8\">\n"
    << "<title>" << title << "</title>\n"
    << "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
    << "<meta name=\"description\" content=\"Przykładowa strona internetowa w c++\">\n"
    << "<meta name=\"author\" content=\"Julia Potaczek\">\n"
    << "<link rel=\"stylesheet\" href=\"styles.css\">\n"
    << "<link rel=\"stylesheet\" href=\"" << path <<".css\">\n"
    << "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,700;1,400&display=swap\">\n"
    << "</head>\n"
    << "<body>\n"
    << "<div class=\"container\">\n"
    << "<div class=\"jumbotron\">\n";
}
void Template::footer() {
    cout << "</div>\n"
    << "<footer div class=\"footer_box\">\n"
    << "<div class=\"footer_navigation\">\n"
    << "<a class=\"footer_navigation_link text-small\" href=\"/\">Strona główna</a>\n"
    << "<a class=\"footer_navigation_link text-small\" href=\"/error.cgi\">Strona błędu 404</a>\n"
    << "</div>\n"
    << "<p class=\"footer_copyright_text text-small\">Copyright &copy; 2020</p>\n"
    << "</footer>\n"
    << "</div>\n"
    << "</body>\n"
    << "</html>\n";
}
