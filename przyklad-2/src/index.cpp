#include "template.hpp"
using namespace std;
int main() {
    Template index;
    string path = getenv("QUERY_STRING");
    index.header("Strona główna", path);
    ifstream html(path + ".html");
    string str((std::istreambuf_iterator<char>(html)), std::istreambuf_iterator<char>());
    cout << str;
    html.close();
    index.footer();
}
