#include "template.hpp"
using namespace std;
int main() {
    Template error;
    error.header("Nie znaleziono", "error");
    cout << "<main>\n"
    << "<p class=\"text-big\">404</p>\n"
    << "<p class=\"text-small\">" << getenv("REQUEST_URI") << "</p>\n"
    << "</main>\n";
    error.footer();
}
